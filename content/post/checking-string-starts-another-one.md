---
title: "Checking if a string starts with another one"
date: 2020-01-06T19:02:04-03:00
draft: false
tags: ["javascript", "native code", "method"]
---

**They say that starting is the hardest part of anything**; with Javascript, fortunately, it doesn't need to be the case. To get started with this blog we'll take a look at the `.startsWith()` method.

Strings are one of the many Javascript's data types. They store a group of characters like letters, symbols or emojis and possess quite a few properties and methods. We'll access these properties and methods using the **dot notation**.

Let's start with the basics of `.startsWith()`: as per the latest [EcmaScript language specification](https://www.ecma-international.org/ecma-262/6.0/#sec-string.prototype.startswith), this method "*returns `true` if the sequence of elements of searchString converted to a String is the same as the corresponding elements of this object (converted to a String) starting at index position. Otherwise returns false*".

Simply put, **indicates wether a string `String` begins with the same content of another string `searchString`** or not. A very basic example would be:

{{< highlight javascript "linenos=table" >}}
// example-startsWith.js

const originalString = "Hello World"
const completelyDifferentString = "🍕 Pepperoni pizza"
const searchString = "Hello"

originalString.startsWith(searchString) // true
originalString.startsWith(completelyDifferentString) // false
{{</ highlight>}}

And while that's its most simplistic use case, there are some other ways we can leverage this method. For that, we'll first check out which parameters can be passed to it and what can be returned

{{< highlight javascript "linenos=table" >}}
/**
 * @param  {searchString} string to search for
 * @param  {number} optional - index, defaults to 0
 * @return {boolean}
 */
string.startsWith(searchString, position)
{{</ highlight>}}

As you can see, the only required value is the string `searchString`
